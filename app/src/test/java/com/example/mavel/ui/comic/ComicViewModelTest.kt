package com.example.mavel.ui.comic

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.mavel.domain.use_cases.ComicUseCase
import io.mockk.impl.annotations.MockK
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ComicViewModelTest {

    @MockK
    lateinit var comicUseCase: ComicUseCase
    private lateinit var comicViewModel: ComicViewModel

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        comicViewModel = ComicViewModel(comicUseCase)
    }

    @Test
    fun `getComicByIdValue with invalid id should set ComicState as error`() {
        comicViewModel.getComicByIdValue("")
        assert(comicViewModel._comicValue.value.error == null)
    }
}