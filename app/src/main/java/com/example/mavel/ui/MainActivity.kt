package com.example.mavel.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import com.example.mavel.R
import com.example.mavel.databinding.ActivityMainBinding
import com.example.mavel.ui.comic.ComicViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val viewModelComic : ComicViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        call()
    }

    private fun call() {
        viewModelComic.getComicByIdValue("1332")
        CoroutineScope(Dispatchers.Main).launch {
            viewModelComic._comicValue.collect() {
                when {
                    it.isLoading -> {
                        binding.progressBar.visibility = View.VISIBLE
                    }
                    it.error.isNotBlank() -> {
                        binding.progressBar.visibility = View.GONE
                        Toast.makeText(this@MainActivity, "Unexpected Error", Toast.LENGTH_LONG).show()
                    }
                    it.comicDetail.isNotEmpty() -> {
                        binding.progressBar.visibility = View.GONE
                        it.comicDetail.map { comic ->
                            val url = "${comic.thumbnail}/landscape_medium.${comic.thumbnailExt}"
                            Log.d("imaget", url)
                            Picasso.with(this@MainActivity).load(url).into(binding.thumbnail)
                            binding.title.text = comic.title
                            binding.description.text = comic.description
                            Log.d("description", comic.description)
                        }
                    }
                }
            }
        }
    }
}