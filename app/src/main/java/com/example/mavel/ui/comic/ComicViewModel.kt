package com.example.mavel.ui.comic

import androidx.lifecycle.ViewModel

import androidx.lifecycle.viewModelScope
import com.example.mavel.domain.use_cases.ComicUseCase
import com.example.mavel.utils.State
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class ComicViewModel @Inject constructor(
    private val comicUseCase: ComicUseCase
) : ViewModel(){

    private val comicValue = MutableStateFlow(ComicState())
    val _comicValue : StateFlow<ComicState> = comicValue

    fun getComicByIdValue(id:String) = viewModelScope.launch{
        comicUseCase(id).collect {
            when(it){
                is State.Success-> {
                    comicValue.value = ComicState(
                        comicDetail = it.data?: emptyList()
                    )
                }
                is State.Loading->{
                    comicValue.value = ComicState(isLoading = true)
                }
                is State.Error->{
                    comicValue.value = ComicState(error = it.message?:"An Unexpected Error")
                }
            }
        }
    }
}