package com.example.mavel.ui.comic

import com.example.mavel.domain.model.ComicModel

data class ComicState(
    val isLoading : Boolean = false,
    val comicDetail : List<ComicModel> = emptyList(),
    val error : String = "error"
)