package com.example.mavel.domain.use_cases

import com.example.mavel.domain.model.ComicModel
import com.example.mavel.domain.repository.MarvelRepository
import com.example.mavel.utils.State
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class ComicUseCase @Inject constructor(
    private val repository : MarvelRepository
) {
    operator fun invoke(id:String): Flow<State<List<ComicModel>>> = flow {
        try {
            emit(State.Loading<List<ComicModel>>())
            val character = repository.getComicById(id).data.results.map {
                it.toComic()
            }
            emit(State.Success<List<ComicModel>>(character))
        }
        catch (e: HttpException){
            emit(State.Error<List<ComicModel>>(e.printStackTrace().toString()))
        }
        catch (e: IOException){
            emit(State.Error<List<ComicModel>>(e.printStackTrace().toString()))
        }
    }
}