package com.example.mavel.domain.repository

import com.example.mavel.data.data_source.dto.ComicDTO

interface MarvelRepository {
    suspend fun getComicById(id:String): ComicDTO
}