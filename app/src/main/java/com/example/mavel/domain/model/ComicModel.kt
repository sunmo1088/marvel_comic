package com.example.mavel.domain.model

data class ComicModel(
    val id : Int,
    val title : String,
    val description : String,
    val thumbnail : String,
    val thumbnailExt: String

)