package com.example.mavel.data

import com.example.mavel.data.data_source.dto.ComicDTO
import com.example.mavel.utils.Constants
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MarvelApi {

    @GET("/v1/public/comics/{comicId}")
    suspend fun getComicById(
        @Path("comicId")comicId:String,
        @Query("ts")ts:String=Constants.timeStamp,
        @Query("apikey")apikey:String=Constants.API_KEY,
        @Query("hash")hash:String= Constants.hash(),
    ): ComicDTO
}