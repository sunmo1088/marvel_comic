package com.example.mavel.data.data_source.dto

data class Creators(
    val available: Int,
    val collectionURI: String,
    val items: List<Any>,
    val returned: Int
)