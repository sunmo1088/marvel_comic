package com.example.mavel.data.data_source.dto

data class Price(
    val price: Double,
    val type: String
)