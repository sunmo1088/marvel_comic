package com.example.mavel.data.data_source.dto

data class Series(
    val name: String,
    val resourceURI: String
)