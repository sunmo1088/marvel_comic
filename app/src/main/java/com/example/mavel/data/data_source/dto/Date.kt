package com.example.mavel.data.data_source.dto

data class Date(
    val date: String,
    val type: String
)