package com.example.mavel.data.repository

import com.example.mavel.data.MarvelApi
import com.example.mavel.data.data_source.dto.ComicDTO
import com.example.mavel.domain.repository.MarvelRepository
import javax.inject.Inject

class MarvelRepositoryImplementation @Inject constructor(
    private val api : MarvelApi
) : MarvelRepository {
    override suspend fun getComicById(id: String): ComicDTO {
        return api.getComicById(id)
    }
}