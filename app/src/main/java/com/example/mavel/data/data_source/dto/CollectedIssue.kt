package com.example.mavel.data.data_source.dto

data class CollectedIssue(
    val name: String,
    val resourceURI: String
)