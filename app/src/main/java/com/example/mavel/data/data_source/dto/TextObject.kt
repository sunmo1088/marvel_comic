package com.example.mavel.data.data_source.dto

data class TextObject(
    val language: String,
    val text: String,
    val type: String
)