package com.example.mavel.data.data_source.dto

data class Image(
    val extension: String,
    val path: String
)