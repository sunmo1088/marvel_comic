# Marvel_Universe

Marvel Comic App using The Marvel API

## Description

A simple app that display comic book image, title and description.


## Tech Stack
- Dagger-Hilt - Used to provide dependency injection
- Retrofit 2 - OkHttp3 - request/response API
- Picasso - for image loading.
- LiveData - use LiveData to see UI update with data changes.
- View Binding - bind UI components in layouts to data sources


